﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class CargarOrdenes
    Inherits System.Web.UI.Page
    Dim shopify As ComunicacionShopify = New ComunicacionShopify()
    Dim dbcon As DbCon = New DbCon()
    Dim correo As AgendarEnvioDeCorreo = New AgendarEnvioDeCorreo()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cargarOrdenesDelDia()

    End Sub
    Protected Sub cargarOrdenesDelDia()
        Dim fechaAMano As String = ConfigurationManager.AppSettings.Get("fechaAMAno").ToString()
        Dim parametros As String
        If (fechaAMano.Equals("")) Then
            parametros = "status=" + ConfigurationManager.AppSettings.Get("status").ToString() + "&fulfillment_status=fulfilled&financial_status=paid&created_at_min=" & Date.Now.ToString("yyyy-MM-dd") & "T00:00:00-4:00"

        Else
            parametros = "status=" + ConfigurationManager.AppSettings.Get("status").ToString() + "&fulfillment_status=fulfilled&financial_status=paid&created_at_min=" & fechaAMano & "T00:00:00-4:00"

        End If
        Try



            Me.bl_respuesta.Items.Add("Consulta hecha:")
            Me.bl_respuesta.Items.Add(ConfigurationManager.AppSettings.Get("urlFarmex").ToString() + parametros)

            Dim ordenesJSON As String = shopify.GetCustomers(parametros)
            Dim correcta As Boolean
            Dim orden As Order
            Dim json As JObject = JValue.Parse(ordenesJSON)
            For Each item As JObject In json("orders")
                orden = New Order()
                correcta = True

                '    Sub Main()
                'Dim name As String = "É"
                Dim utf8 As New UTF8Encoding()
                Dim encodedBytes As Byte()
                'Dim encodedBytes As Byte() = utf8.GetBytes(name)
                '    Dim decodedString As String = utf8.GetString(encodedBytes)
                'End Sub

                Try
                    If (item.GetValue("order_number").ToString().Equals("")) Then
                        orden.name = " "
                        correcta = False
                    Else
                        orden.name = item.GetValue("order_number").ToString()
                    End If
                    If (item.GetValue("created_at").ToString().Equals("")) Then
                        orden.created_at = DateTime.Now
                        correcta = False
                    Else
                        orden.created_at = item.GetValue("created_at").ToString()
                    End If
                    If (item.GetValue("gateway").ToString().Equals("")) Then
                        orden.paymentMethod = " "
                    Else
                        orden.paymentMethod = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("gateway").ToString()))
                    End If
                    If (item.GetValue("phone").ToString().Equals("")) Then
                        orden.telefono = " "
                    Else
                        orden.telefono = item.GetValue("phone").ToString()
                    End If
                    If (item.GetValue("total_weight").ToString().Equals("")) Then
                        orden.total_weight = 1
                    Else
                        orden.total_weight = item.GetValue("total_weight").ToString()
                    End If
                    If (item.GetValue("total_weight").ToString().Equals("")) Then
                        orden.total_weight = " "
                    Else
                        orden.total_weight = item.GetValue("total_weight").ToString()
                    End If
                    If (item.GetValue("contact_email").ToString().Equals("")) Then
                        orden.email = " "
                    Else
                        orden.email = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("contact_email").ToString()))
                    End If

                    If (item.GetValue("shipping_address")("name").ToString().Equals("")) Then
                        orden.ShippingName = " "
                        correcta = False
                    Else
                        orden.ShippingName = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("shipping_address")("name").ToString()))
                        'encodedBytes = utf8.GetBytes(item.GetValue("shipping_address")("name").ToString())
                        'orden.ShippingName = utf8.GetString(encodedBytes)
                    End If

                    If (item.GetValue("shipping_address")("address1").ToString().Equals("")) Then
                        orden.shippingAdress = " "
                        correcta = False
                    Else
                        orden.shippingAdress = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("shipping_address")("address1").ToString()))

                        'orden.shippingAdress = item.GetValue("shipping_address")("address1").ToString()
                    End If

                    If (item.GetValue("shipping_address")("address2").ToString().Equals("")) Then
                        orden.comuna = " "
                        correcta = False
                    Else
                        orden.comuna = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("shipping_address")("address2").ToString()))

                    End If
                    If (item.GetValue("shipping_address")("province_code").ToString().Equals("")) Then
                        orden.region = " "
                        correcta = False
                    Else
                        orden.region = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("shipping_address")("province_code").ToString()))
                    End If

                    If (item.GetValue("billing_address")("name").ToString().Equals("")) Then
                        orden.billingName = " "
                        correcta = False
                    Else
                        orden.billingName = System.Text.Encoding.UTF8.GetString(Encoding.Default.GetBytes(item.GetValue("billing_address")("name").ToString()))
                    End If

                    If (correcta) Then
                        dbcon.insertaOrdenesShopify(orden, ConfigurationManager.AppSettings.Get("user").ToString())
                    Else
                        dbcon.insertaOrdenesShopifyErroneas(orden, ConfigurationManager.AppSettings.Get("user").ToString())
                    End If
                    Me.bl_respuesta.Items.Add("Ref: " + orden.name + " ")
                Catch ex As Exception
                    Me.bl_respuesta.Items.Add("Ref: " + orden.name + " Error")

                End Try

            Next
        Catch ex As Exception
            envioCorreo(ConfigurationManager.AppSettings.Get("urlFarmex").ToString() + parametros)
        End Try

    End Sub

    Public Sub envioCorreo(ruta As String)
        Dim jsonString As String

        jsonString = "[{" + """para"":""jnavarro@shipex.cl"","
        jsonString = jsonString & """sistema"":""Toma de ordenes Shopify"","
        jsonString = jsonString & """configId"": " + ConfigurationManager.AppSettings.Get("configIdMail").ToString() + ","
        jsonString = jsonString & """valVariable"": ""{'@@url':'" & ruta & "'}" & """}]"
        correo.agregarEnvio(jsonString)

    End Sub



End Class
