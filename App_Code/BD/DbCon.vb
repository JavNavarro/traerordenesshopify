﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class DbCon

    Public Function traerCiudadPorComuna(Comuna As String) As String


        Dim transaction As SqlTransaction

        Dim ciudad As String
        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            cnn.Open()


            Dim cmd As New SqlCommand("traerCiudadSegunComuna", cnn)

            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add(New SqlParameter("comuna", Comuna.ToUpper))

            Dim result As SqlDataReader = cmd.ExecuteReader
            If result.HasRows Then
                While result.Read
                    ciudad = result.GetString(0)
                End While
            End If
            result.Close()
            cnn.Close()
        End Using
        Return ciudad
    End Function

    Public Function TraeCodComuna(comuna As String) As Int32

        Dim dt As DataTable = New DataTable()

        Dim correlativo As Int32

        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_CodComuna", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("comuna", comuna))

            Dim result As SqlDataReader = cmd.ExecuteReader

            dt.Load(result)


            result.Close()

            correlativo = dt.Rows(0)(0).ToString

            cnn.Close()

        End Using
        Return correlativo
    End Function
    Public Function GetPrefijoCliente(id As Integer) As Integer
        Dim despachoEncontrado As Integer
        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            cnn.Open()
            Dim cmd As New SqlCommand("Shipex_ObtenerPrefijo", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("id", id))
            Dim resultado As SqlDataReader = cmd.ExecuteReader
            If resultado.HasRows Then
                resultado.Read()

                despachoEncontrado = resultado(0)
            End If
            cnn.Close()
        End Using
        Return despachoEncontrado
    End Function
    Public Function TraeCorrelativo(cli_id As Int32) As Int32

        Dim dt As DataTable = New DataTable()
        Dim correlativo As Int32

        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            cnn.Open()

            Dim cmd As New SqlCommand("Shipex_UsarCorrelativousuario", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("idUser", cli_id))

            Dim result As SqlDataReader = cmd.ExecuteReader

            dt.Load(result)


            result.Close()

            correlativo = dt.Rows(0)(0).ToString
            cnn.Close()


        End Using
        Return correlativo
    End Function


    Public Function traerOrdenes(User As String, fecha As Date, estado As Integer) As DataTable
        Dim lista As DataTable = New DataTable()
        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            Try

                cnn.Open()
                Dim cmd As SqlCommand

                cmd = New SqlCommand("TraerOrdenesShopify", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("user", User))
                cmd.Parameters.Add(New SqlParameter("fecha", fecha))
                cmd.Parameters.Add(New SqlParameter("estado", estado))
                Dim reader As SqlDataReader = cmd.ExecuteReader
                If reader.Read() Then
                    lista.Load(reader)
                End If
                reader.Close()
                cnn.Close()

            Catch ex As Exception
                cnn.Close()
            End Try
        End Using

        Return lista
    End Function


    Public Function traerOrdenesErroneas(User As String, fecha As Date) As DataTable
        Dim lista As DataTable = New DataTable()
        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            Try

                cnn.Open()
                Dim cmd As SqlCommand

                cmd = New SqlCommand("TraerOrdenesShopifyErroneas", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("user", User))
                cmd.Parameters.Add(New SqlParameter("fecha", fecha))
                Dim reader As SqlDataReader = cmd.ExecuteReader
                If reader.Read() Then
                    lista.Load(reader)

                End If
                reader.Close()
                cnn.Close()

            Catch ex As Exception
                cnn.Close()
            End Try
        End Using

        Return lista
    End Function



    Public Function insertaOrdenesShopify(ordenShopify As Order, user As String) As Integer
        Dim rows As Integer
        rows = 0
        Dim err As String

        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            Try
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("InsertarOrdenesShopify", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("referenciaCliente", ordenShopify.name))
                cmd.Parameters.Add(New SqlParameter("createdAt", ordenShopify.created_at))
                cmd.Parameters.Add(New SqlParameter("paymentMethod", ordenShopify.paymentMethod))
                cmd.Parameters.Add(New SqlParameter("billingName", ordenShopify.billingName))
                cmd.Parameters.Add(New SqlParameter("telefono", ordenShopify.telefono))
                cmd.Parameters.Add(New SqlParameter("region", ordenShopify.region))
                cmd.Parameters.Add(New SqlParameter("comuna", ordenShopify.comuna))
                cmd.Parameters.Add(New SqlParameter("shippingAdress", ordenShopify.shippingAdress))
                cmd.Parameters.Add(New SqlParameter("peso", ordenShopify.total_weight))
                cmd.Parameters.Add(New SqlParameter("nombreCliente", ordenShopify.ShippingName))
                cmd.Parameters.Add(New SqlParameter("email", ordenShopify.email))
                cmd.Parameters.Add(New SqlParameter("estado", 1))
                cmd.Parameters.Add(New SqlParameter("Usuario", user))

                rows = cmd.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                err = ex.ToString
                rows = 0
                cnn.Close()
            End Try
        End Using

        Return rows
    End Function

    Public Function insertaOrdenesShopifyErroneas(ordenShopify As Order, user As String) As Integer
        Dim rows As Integer
        rows = 0
        Dim err As String
        Try
            Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("InsertarOrdenesShopifyConError", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("referenciaCliente", ordenShopify.name))
                cmd.Parameters.Add(New SqlParameter("createdAt", ordenShopify.created_at))
                cmd.Parameters.Add(New SqlParameter("paymentMethod", ordenShopify.paymentMethod))
                cmd.Parameters.Add(New SqlParameter("billingName", ordenShopify.billingName))
                cmd.Parameters.Add(New SqlParameter("telefono", ordenShopify.telefono))
                cmd.Parameters.Add(New SqlParameter("region", ordenShopify.region))
                cmd.Parameters.Add(New SqlParameter("comuna", ordenShopify.comuna))
                cmd.Parameters.Add(New SqlParameter("shippingAdress", ordenShopify.shippingAdress))
                cmd.Parameters.Add(New SqlParameter("peso", ordenShopify.total_weight))
                cmd.Parameters.Add(New SqlParameter("nombreCliente", ordenShopify.ShippingName))
                cmd.Parameters.Add(New SqlParameter("email", ordenShopify.email))
                cmd.Parameters.Add(New SqlParameter("Usuario", user))

                rows = cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            err = ex.ToString
            rows = 0
        End Try
        Return rows
    End Function
    Public Function TraerUsuario(user_id As Integer) As String
        Dim user As String = ""


        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            Try
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("Shipex_ObtenerUsuario", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("idUser", user_id))
                Dim reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    user = reader("nombre")
                End While
                cnn.Close()

            Catch ex As Exception
                cnn.Close()
            End Try
        End Using
        Return user
    End Function


    Public Function traerEtiquetas(codDespacho As Long) As List(Of String)
        Dim lista As List(Of String) = New List(Of String)
        Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
            Try
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("Shipex_ObtenerEtiquetas", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("codDespacho", codDespacho))
                Dim reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read()
                    lista.Add(reader("url"))
                End While
                cnn.Close()
            Catch ex As Exception
                cnn.Close()
            End Try


        End Using
        Return lista

    End Function

    Public Function PasarOrdenAProcesada(user As String, referenciaCliente As String) As Integer
        Dim rows As Integer
        rows = 0
        Dim err As String
        Try
            Using cnn As New SqlConnection(ConfigurationManager.AppSettings.Get("dbShipex").ToString)
                cnn.Open()
                Dim cmd As SqlCommand
                cmd = New SqlCommand("ActualizarOrdenAProcesada", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New SqlParameter("referenciaCliente", referenciaCliente))
                cmd.Parameters.Add(New SqlParameter("Usuario", user))

                rows = cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            err = ex.ToString
            rows = 0
        End Try
        Return rows
    End Function


End Class
