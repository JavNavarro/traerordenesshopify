﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json

Public Class ComunicacionShopify
    Dim correo As AgendarEnvioDeCorreo = New AgendarEnvioDeCorreo()
    Public Function GetCustomers(parametros As String) As String

        Dim result As String = ""
        Try


            Dim url As String = ConfigurationManager.AppSettings.Get("urlFarmex").ToString() + parametros
            Dim client As WebClient = New WebClient()
            Dim userName As String = ConfigurationManager.AppSettings.Get("userFarmex").ToString()
            Dim passWord As String = ConfigurationManager.AppSettings.Get("passFarmex").ToString()
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Dim credentials As String = Convert.ToBase64String(Encoding.ASCII.GetBytes(userName & ":" + passWord))
            client.Headers(HttpRequestHeader.Authorization) = "Basic " & credentials
            result = client.DownloadString(url)
            ' Response.Write(result)

        Catch ex As Exception
            correo.envioCorreo(ConfigurationManager.AppSettings.Get("urlFarmex").ToString() + parametros)
        End Try
        Return result
    End Function






End Class
