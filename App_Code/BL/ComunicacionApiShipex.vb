﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json

Public Class ComunicacionApiShipex



    Public Function GenerarEtiqueta(json As String, datos As Guia) As String

        Dim resultado As String = ""
        Try
            Dim jsonstring As List(Of Guia) = New List(Of Guia)
            jsonstring = JsonConvert.DeserializeObject(Of List(Of Guia))(json)
            Dim Consulta_API As String = ConfigurationManager.AppSettings.Get("Consulta_API")
            Dim URL As String = Consulta_API
            Dim postData As String = JsonConvert.SerializeObject(jsonstring)
            Dim Bts As Byte() = Encoding.UTF8.GetBytes(postData)
            Dim httpWebRequest1 As WebRequest = WebRequest.Create(URL)
            httpWebRequest1.Method = "POST"
            httpWebRequest1.ContentLength = Bts.Length
            httpWebRequest1.ContentType = "text/json"
            Using requestStream As Stream = httpWebRequest1.GetRequestStream()
                requestStream.Write(Bts, 0, Bts.Count())
            End Using
            Dim respon_se As WebResponse = httpWebRequest1.GetResponse()
            Dim dataStream As Stream = respon_se.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            Dim ValLenght As Integer = responseFromServer.Length - 2
            Dim ValPreFinal As String = responseFromServer.Substring(1, ValLenght)
            Dim Final As String = ValPreFinal.Replace("\", String.Empty)
            Dim ListaResultado As List(Of RespuestaShipex) = New List(Of RespuestaShipex)
            ListaResultado = JsonConvert.DeserializeObject(Of List(Of RespuestaShipex))(Final)
            reader.Close()
            respon_se.Close()
            For Each item As RespuestaShipex In ListaResultado
                If item.Respuesta.Length > 0 And item.ReferenciaCliente.Length > 0 And item.Referencia.Length > 0 And item.urlEtiqueta.Length > 0 Then
                    '  band = True
                End If
                'link_etiqueta.Items.Add(item.urlEtiqueta)
                'link_etiqueta.DataBind()

                Dim variable As String = item.urlEtiqueta
                resultado = variable
                ' Me.ImageButton1.ImageUrl = variable

            Next
        Catch ex As Exception
            resultado = "Error: No se pudo generar la etiqueta. Verifique el archivo."
        End Try

        Return resultado
    End Function

End Class
