﻿Imports System
Imports Microsoft.VisualBasic

Public Class Order
    Private _id As Int64
    Public Property id() As Int64
        Get
            Return _id
        End Get
        Set(ByVal value As Int64)
            _id = value
        End Set
    End Property
    Private _email As String
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    'Private _closed_at As DateTime
    'Public Property closed_at() As DateTime
    '    Get
    '        Return _closed_at
    '    End Get
    '    Set(value As DateTime)
    '        _closed_at = value
    '    End Set
    'End Property
    Private _created_at As DateTime
    Public Property created_at() As DateTime
        Get
            Return _created_at
        End Get
        Set(value As DateTime)
            _created_at = value
        End Set
    End Property
    'Private _updated_at As DateTime
    'Public Property updated_at() As DateTime
    '    Get
    '        Return _updated_at
    '    End Get
    '    Set(value As DateTime)
    '        _updated_at = value
    '    End Set
    'End Property
    Private _number As Int32
    Public Property number() As Int32
        Get
            Return _number
        End Get
        Set(value As Int32)
            _number = value
        End Set
    End Property
    'Private _note As String
    'Public Property note() As String
    '    Get
    '        Return _note
    '    End Get
    '    Set(value As String)
    '        _note = value
    '    End Set
    'End Property
    'Private _token As String
    'Public Property token() As String
    '    Get
    '        Return _token
    '    End Get
    '    Set(value As String)
    '        _token = value
    '    End Set
    'End Property
    'Private _gateway As String
    'Public Property gateway() As String
    '    Get
    '        Return _gateway
    '    End Get
    '    Set(value As String)
    '        _gateway = value
    '    End Set
    'End Property
    'Private _test As String
    'Public Property test() As String
    '    Get
    '        Return _test
    '    End Get
    '    Set(value As String)
    '        _test = value
    '    End Set
    'End Property
    Private _total_price As String
    Public Property total_price() As String
        Get
            Return _total_price
        End Get
        Set(value As String)
            _total_price = value
        End Set
    End Property
    'Private _subtotal_price As String
    'Public Property subtotal_price() As String
    '    Get
    '        Return _subtotal_price
    '    End Get
    '    Set(value As String)
    '        _subtotal_price = value
    '    End Set
    'End Property
    Private _total_weight As Double
    Public Property total_weight() As Double
        Get
            Return _total_weight
        End Get
        Set(value As Double)
            _total_weight = value
        End Set
    End Property
    Private _billingName As String
    Public Property billingName() As String
        Get
            Return _billingName
        End Get
        Set(value As String)
            _billingName = value
        End Set
    End Property
    Private _telefono As String
    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property
    Private _region As String
    Public Property region() As String
        Get
            Return _region
        End Get
        Set(value As String)
            _region = value
        End Set
    End Property
    Private _paymentMethod As String
    Public Property paymentMethod() As String
        Get
            Return _paymentMethod
        End Get
        Set(value As String)
            _paymentMethod = value
        End Set
    End Property
    Private _confirmed As Boolean
    Public Property confirmed() As Boolean
        Get
            Return _confirmed
        End Get
        Set(value As Boolean)
            _confirmed = value
        End Set
    End Property
    Private _total_discounts As String
    Public Property total_discounts() As String
        Get
            Return _total_discounts
        End Get
        Set(value As String)
            _total_discounts = value
        End Set
    End Property
    Private _total_line_items_price As String
    Public Property total_line_items_price() As String
        Get
            Return _total_line_items_price
        End Get
        Set(value As String)
            _total_line_items_price = value
        End Set
    End Property
    Private _cart_token As String
    Public Property cart_token() As String
        Get
            Return _cart_token
        End Get
        Set(value As String)
            _cart_token = value
        End Set
    End Property
    Private _buyer_accepts_marketing As Boolean
    Public Property buyer_accepts_marketing() As Boolean
        Get
            Return _buyer_accepts_marketing
        End Get
        Set(value As Boolean)
            _buyer_accepts_marketing = value
        End Set
    End Property
    Private _name As String 'RefCliente
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Private _landing_site As String
    Public Property landing_site() As String
        Get
            Return _landing_site
        End Get
        Set(value As String)
            _landing_site = value
        End Set
    End Property
    Private _cancelled_at As DateTime
    Public Property cancelled_at() As DateTime
        Get
            Return _cancelled_at
        End Get
        Set(value As DateTime)
            _cancelled_at = value
        End Set
    End Property
    Private _ShippingName As String
    Public Property ShippingName() As String
        Get
            Return _ShippingName
        End Get
        Set(value As String)
            _ShippingName = value
        End Set
    End Property
    Private _total_price_usd As String
    Public Property total_price_usd() As String
        Get
            Return _total_price_usd
        End Get
        Set(value As String)
            _total_price_usd = value
        End Set
    End Property
    Private _checkout_token As String
    Public Property checkout_token() As String
        Get
            Return _checkout_token
        End Get
        Set(value As String)
            _checkout_token = value
        End Set
    End Property
    Private _reference As String
    Public Property reference() As String
        Get
            Return _reference
        End Get
        Set(value As String)
            _reference = value
        End Set
    End Property
    Private _user_id As String
    Public Property user_id() As String
        Get
            Return _user_id
        End Get
        Set(value As String)
            _user_id = value
        End Set
    End Property
    Private _location_id As String
    Public Property location_id() As String
        Get
            Return _location_id
        End Get
        Set(value As String)
            _location_id = value
        End Set
    End Property
    Private _shippingAdress As String
    Public Property shippingAdress() As String
        Get
            Return _shippingAdress
        End Get
        Set(value As String)
            _shippingAdress = value
        End Set
    End Property
    Private _source_url As String
    Public Property source_url() As String
        Get
            Return _source_url
        End Get
        Set(value As String)
            _source_url = value
        End Set
    End Property
    Private _processed_at As DateTime
    Public Property processed_at() As DateTime
        Get
            Return _processed_at
        End Get
        Set(value As DateTime)
            _processed_at = value
        End Set
    End Property
    Private _comuna As String
    Public Property comuna() As String
        Get
            Return _comuna
        End Get
        Set(value As String)
            _comuna = value
        End Set
    End Property




End Class
